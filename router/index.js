 'use strict'

var express = require('express');
var router = express.Router();
var control = require('../controller/control.js');



//para la pagina de usuario
//retorna todos los campos base de datos dan segun el id
router.get('/user/:id', control.userDetail);
//when activate a toggle
router.post('/user/available', control.createChatUser);
//cuando desactivamos un toggle
router.delete('/user/unavailable/:id', control.deleteChatUser);
//recibir toda la lista de activos
//retorna una lista 
router.get('/profesional-list', control.profesionalList);
//get el user de la lista para ver su bio etc
//se le generara un id para el chat
router.get('/profesional-list/:id', control.profesionalListDetail);
//creacion del chat
//creamos uuids para ambos y una room
router.post('/chat-list/new-chat', control.chatNewRom);
//Chat list
router.get('/chat-list/:id', control.getChatList);
//router get chat
router.get('/chat-list/chat/:id', control.getChatDetail);

module.exports = router;