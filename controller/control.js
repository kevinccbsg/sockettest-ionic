'use strict'
const uuid = require('uuid');
const helper = require('../config/helperFunctions');
/////Fake database to chat
var fakeDDBB = require('../config/fakeDDBB');

exports.userDetail = (req, res, next) => {
	let idUser = req.params.id;
	if (idUser === "1") {
		helper.success(res, next, fakeDDBB.myuser);
	}
	else if (idUser === "2") {
		helper.success(res, next, fakeDDBB.otherUser);
	}
	else {
		helper.error(res, next, "No user found", 404);
	}
}
exports.createChatUser = (req, res, next) => {
	console.log("createChatUser");
	let new_uuid = uuid.v1();
	let userActive = {
		id: new_uuid,
		name: req.body.name,
		last_name: req.body.last_name,
		username: req.body.username,
		img: req.body.img,
		titles: req.body.titles,
		languajes: req.body.languajes,
		sexo: req.body.sexo,
		bio: req.body.bio
	};
	fakeDDBB.usersActive.push(userActive);
	helper.success(res, next, fakeDDBB.usersActive);
}
exports.deleteChatUser = (req, res, next) => {
	console.log("deleteChatUser");
}
exports.profesionalList = (req, res, next) => {
	console.log("profesionalList");
	helper.success(res, next, fakeDDBB.usersActive);
}
exports.profesionalListDetail = (req, res, next) => {
	console.log("profesionalListDetail");
	let idUser = req.params.id;
	let senUser = fakeDDBB.usersActive.filter((obj) => {
		return obj.id === idUser ? obj : null;
	});
	if (senUser != null) {
		helper.success(res, next, senUser);
	}
	else {
		helper.error(res, next, "No user found", 404);
	}
}
exports.chatNewRom = (req, res, next) => {
	console.log("chatNewRom");
	let chat = {
		idUser: req.body.idUser,
		id: req.body.id,
		name: req.body.name,
		username: req.body.username,
		img: req.body.img,
		messages: []
	};
	fakeDDBB.chatRooms.push(chat);
	helper.success(res, next, fakeDDBB.chatRooms);
}
exports.getChatList = (req, res, next) => {
	console.log("getChatList");
	let idUser = req.params.id;
	let sendUser = fakeDDBB.chatRooms.filter((obj) => {
		return obj.idUser === idUser ? obj : null;
	});
	if (sendUser != null) {
		helper.success(res, next, sendUser);
	}
	else {
		helper.error(res, next, "No user found", 404);
	}

}
exports.getChatDetail = (req, res, next) => {
	console.log("getChatDetail");
	let idUser = req.params.id;
	let sendUser = fakeDDBB.chatRooms.filter((obj) => {
		return obj.id === idUser ? obj : null;
	});
	if (sendUser != null) {
		helper.success(res, next, sendUser);
	}
	else {
		helper.error(res, next, "No user found", 404);
	}
}