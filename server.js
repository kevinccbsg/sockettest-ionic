var express = require('express');
var app = express();
var bodyParser = require('body-parser')
var http = require('http').Server(app);
var realtime = require('./sockets');
var router = require('./router');
var morgan = require('morgan');
var fakeDDBB = require('./config/fakeDDBB');

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
//app.use(morgan('combined'));
app.use(express.static(__dirname + '/public'))

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

app.disable('x-powered-by');

app.get('/logIn/:username', (req, res, next) => {
	//if user exist retorno para que se puede acceder y el id de mi user
	var variableToCompare = req.params.username;
	if ( variableToCompare === "1" ) {
		res.setHeader("content-Type","application/json");
		res.writeHead(200);
		res.end(JSON.stringify({
			status: "done",
			data: {
				id: fakeDDBB.myuser.id,
				name: fakeDDBB.myuser.name,
				last_name: fakeDDBB.myuser.last_name,
				username: fakeDDBB.myuser.username,
				img: fakeDDBB.myuser.img,
				titles: fakeDDBB.myuser.titles,
				languajes: fakeDDBB.myuser.languajes,
				sexo: fakeDDBB.myuser.sexo,
				bio: fakeDDBB.myuser.bio
			}
		}));
		return next();
	}
	else if ( variableToCompare === "2" ) {
		res.setHeader("content-Type","application/json");
		res.writeHead(200);
		res.end(JSON.stringify({
			status: "done",
			data: {
				id: fakeDDBB.otherUser.id,
				name: fakeDDBB.otherUser.name,
				last_name: fakeDDBB.otherUser.last_name,
				username: fakeDDBB.otherUser.username,
				img: fakeDDBB.otherUser.img,
				titles: fakeDDBB.otherUser.titles,
				languajes: fakeDDBB.otherUser.languajes,
				sexo: fakeDDBB.otherUser.sexo,
				bio: fakeDDBB.otherUser.bio
			}
		}));
		return next();
	}
	else {
		res.setHeader("content-Type","application/json");
		res.writeHead(404);
		res.end(JSON.stringify({error: "No exite ese user"}));
		return next();
	}
});

//Api Endpoints
app.use('/api', router);

realtime(http);

http.listen(3000, function(){
  console.log('listening on *:3000');
});
