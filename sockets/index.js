'use strict'

const socketio = require('socket.io');
/////Fake database to chat
var fakeDDBB = require('../config/fakeDDBB');

module.exports = function (server) {
	
	const io = socketio(server);

	// Logicas del chat
	io.on('connection', function(socket){

		console.log('a user connected'+socket.id);
		socket.on('disconnect', function() {
			console.log('user disconnect'+socket.id)
		});


		socket.on('chat message',function(msg) {
			var formatedData = {
				owner: msg.owner,
				username: msg.username,
				message: msg.message
			};
			fakeDDBB.chatRooms[0].messages.push(formatedData);
			console.log(msg);
			io.sockets.emit('chat message',formatedData);
		});
	});
}