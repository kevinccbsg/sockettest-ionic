(function() {
	var app = angular.module('app',[]);

	app.factory('socket', ['$rootScope', function($rootScope) {
		var socket = io.connect();
		  return {
		    on: function (eventName, callback) {
		      socket.on(eventName, function () {
		        var args = arguments;
		        $rootScope.$apply(function () {
		          callback.apply(socket, args);
		        });
		      });
		    },
		    emit: function (eventName, data, callback) {
		      socket.emit(eventName, data, function () {
		        var args = arguments;
		        $rootScope.$apply(function () {
		          if (callback) {
		            callback.apply(socket, args);
		          }
		        });
		      })
		    }
		  };
	}]);

	app.controller('IndexController', function($scope, socket) {
		$scope.items = [];//tirar de la base de datos y cargar el valor
	    $scope.add = function(item) {
	        var sendText = item;
	        console.log($scope.items)
	        socket.emit('chat message',sendText);
	    };
		socket.on('chat message', function(msg) {
			console.log("REcibo");
			$scope.items.push(msg);
			console.log($scope.items);
		})
	});
})();