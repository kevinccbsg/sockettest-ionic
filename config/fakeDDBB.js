module.exports.myuser = {
	id: "1",
	name: "Kevin",
	last_name: "Martinez",
	username: "ccbsg",
	img: "https://i1.social.s-msft.com/profile/u/avatar.jpg?displayname=max%20from%20ionic&size=extralarge&version=582edb9c-384c-4ea5-98b7-3f62199aadbb",
	titles: ["cocinero","Armador"],
	languajes: ["Español", "Ingles"],
	sexo: "Hombre",
	bio: "Desarrollador web. Node Ionic ES6"
}

module.exports.otherUser = {
	id: "2",
	name: "Julia",
	last_name: "Escobar",
	username: "ccbsg_patron",
	img: "https://a.disquscdn.com/uploads/users/11541/1671/avatar92.jpg?1447690563",
	titles: ["Patron","Armador"],
	languajes: ["Español", "Chino"],
	sexo: "Mujer",
	bio: "Desarrolladora web. Angular, backbone .net"
}
module.exports.currentlyUser = {}
module.exports.usersActive = []
module.exports.chatRooms = []